# Introduction
## Slide 12
Separation of concerns is a generic term, used all over the software development domain. It's all about a simple sanity rule: keep things separated, conceptually and visually, if they address different concerns. By keeping things separated, we don't really mean putting this:

## Slide 13
Suspense ...

## Slide 14
Any non-trivial application is composed of modules, files, components, all addressing various concerns. It all needs proper structure. We can identify two distinct areas -

* Application - abstract structure of your application, in terms of interacting components
* Project - the physical structure of your application, including the above, but also other elements

## Application - logical structure

* Domain model - definition of entities processed
* Business logic - components performing processes (login/logout, checkout, data transformations)
* Internal services - generic services for data access, network communications, encryption
* UI logic - components controlling your UI
* UI views - the visual components
* APIs - endpoints allowing others utilize your app

## Project - physical structure
* Source code
* Configuration
* Tests
* Deployment scripts
* Documentation

All this needs to be logically structured. If not, the application will quickly become unmanageable. It doesn't mean it will not work. Bad code is like virus, it has many survival strategies. Most common is write-only code. New developers start rewriting parts of the code from scratch. Costly as it is, it's certainly less frustrating than attempts to decipher mess inherited from the past. This is one of reasons for proliferation of frameworks. Newcomers find too hard to comprehend the bloat, and so they introduce their own bloat.

This is rougly how we got Windows 10.

# Recommendations
## Slide 15
1. Keep programming languages **separated**, whatever React guys say. Your graphic designer will be forever thankful.

2. Use methodology intended for the task, even if alternatives are cute

- Keep JavaScript out of HTML
- Keep CSS out of HTML
- Keep HTML out of JavaScript
- Keep CSS out of JavaScript

I can see React developers raising their pitchforks :-) Before you burn me, just remember: this DOESN'T MEAN these things have to be in **separate** files. I mean rather that:

- JavaScript shouldn't spit out HTML strings, bt rather use HTML templating
- JavaScript shouldn't fiddle with CSS attributes of DOM elements but rather toggle classes
- HTML markup shouldn't have inline CSS attributes

Riot.js is a good example of that. A Riot component **can** be cheerfully contained in a single file and it does make sense if it's just a tiny bit with no complex markup and styling. Still, Riot component structure neatly separates HTML from CSS from JS.

3. Structure your application by **feature**, not by layers. In old MVC days we'd have:

    /model
        checkout.js
        summary.js
    /controllers
        checkout.js
        summary.js
    /views
        checkout.html
        summary.html
    /styles
        checkout.css
        summary.css
    /scripts
        data.js
        login.js
    index.html

Wrong. Any feature you implement is spread over multiple folders, difficult to grasp if you're new to the team. How about:

    /components
        /checkout
            checkout.js
            checkout-view.js
            checkout-view.css
            checkout-view.html
        /summary
            summary.js
            summary-view.html
            summary-view.css
            summary-view.js
    /services
        data.js
        login.js
    index.html

A quick look at this makes you instantly informed about the structure and the purpose of your application. Bug on checkout page? Most of the related code is right there, easy to find.

