# DRY
Which is why we use frameworks.

# YAGNI and KISS
But ... They come bloated and with futures you'll never use. Other challenges:

- When the framework is 50 times larger than your actual app code. Something ain't right, when npm pulls hundreds of files for just a hello-world app. Something ain't right, when hundreds kB need to be downloaded just for an empty start page.
- Black magic behaviour, which strikes with vengeance in border cases and errorenous situations
- Too many layers of abstraction, perfectly visible in Angular stack traces. Just try making sense of these...
- Ritualistic behaviours and impenetrable terminology:
    - **Angular**: scope, digest, rootscope, emit, dependency injection, broadcast, transclude, factory, bootstrapping, decorator
    - **React/Flux**: dispatcher, unidirectional, immutable, store, state transition, jsx, isomorphic, props
  It's a huge cognitive effort, and in the end you turn into another incomprensible zombie speaking weird language, simply unable to explain the technology behind his application.

Recommended reading:
    [https://muut.com/blog/technology/frameworkless-javascript.html]()


# It's not so much about specific framework, but components, components, components
Riot.js makers say on their website: "

> We should focus on reusable components instead of templates.
> According to the developers of React, templates separate technologies, not concerns.
> By having related layout and logic together under the same component the overall
> system becomes cleaner. We respect React for this important insight.

## React
But what did the React developers did subsequently? React developers said: "To hell with the W3C and to hell with best practices!" and decided to completely abstract away the browser, add HTML to their JavaScript files, dogs and cats living together, mass hysteria.

## Ember, Knockout, Backbone
How about Ember or Knockout or Backbone? They pretty much require that you rewrite your domain model using THEIR observables, Ember.object wrappers etc., to enable things like data bindings. As our sample data binding implementation shows - rather unnecessary, as any wrapping, if required at all, can be done by the framework behind the screens. Once we thought even that won't be necessary with Object.observe, but it just got obsoleted and now the [https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Proxy](Proxy object) is coming as a replacement.

## AngularJS
AngularJS is an interesting example of a framework, that the makers seemingly didn't quite realise how to use it properly :-) If you learn AngularJS using the official tutorials, you're already doing it wrong, period. Things like scopes, emits and broadcasts, views and controllers and ng-include - they **might** be needed, but you can write extremely complex and rich UIs entirely without them, using just the two fundamental provisions:

- component for decomposing UI
- singleton services for always-in-sync application state

## JQuery
It's a blasphemy these days to declare in public: *"I'm using JQuery in my app"*. Common belief is that using JQuery leads to spaghetti code. This has nothing to do with JQuery. This has certainly to do with "developers" without any notion of separation of concerns, who joyfully code their view logic, their domain model and business processes all in one place, mixed together with HTML and CSS. If you organise your code the right way, there's nothing wrong with JQuery. But to be able to do that, one needs to study the principles of programming, rather than stealing code snippets from StackOverflow. The worst thing you can do is to ditch JQuery for these wrong reasons, as you get rid of a great and well designed framework that made the current web possible!

## Angular 2/3/4/whatever
It has as much in common with Angular 1.* as organic farming with string theory. It could have been called **Bob** just as well. Then, why o why is there:

- an @ in front of @Component
- {{ }} but also [(ngModel)]
- and why [( )] ? Looks cute, sure, but ...
- brackets around (ngModelChange)=""
- and * in front of *ngIf
- and all of a sudden lowerCamelCase is allowed, while in HTML we use kebab-case, period, what's what we were told for 2 years of AngularJS!?

And have you seen the boilerplate apps? Here's one I love: [https://github.com/AngularClass/angular2-webpack-starter]()

>🎉 An Angular Starter kit featuring Angular 2 and Angular 4 (Router, Http,
>Forms, Services, Tests, E2E, Dev/Prod, HMR, Async/Lazy Routes, AoT via ngc),
>Karma, Protractor, Jasmine, Istanbul, TypeScript 2, TsLint, Codelyzer,
>Hot Module Replacement, @types, and Webpack 2.

Jaysus.

# So whatever you choose, it will hurt in the end
You can only try to make the pain less, by making reasonable choices. Just remember - frameworks come and go, while classic programming principles last pretty much forever.