export class Application {
    constructor(container) {
        this.container = container;
    }

    title: string;
    version: string;

    initialize(title: string, version: string) {
        this.title = title;
        this.version = version;
        this.initialized = true;
    }

    private container: string = null;
    private initialized: Boolean = false;
}