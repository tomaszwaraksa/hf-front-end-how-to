"use strict";
var Application = (function () {
    function Application(container) {
        this.container = null;
        this.initialized = false;
        this.container = container;
    }
    Application.prototype.initialize = function (title, version) {
        this.title = title;
        this.version = version;
        this.initialized = true;
    };
    return Application;
}());
exports.Application = Application;
