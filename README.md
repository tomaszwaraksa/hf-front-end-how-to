# Slides
https://docs.google.com/presentation/d/13C32nUkwL4Fc2jAgM84_PAR8NjAZQCtDyXhtks-mtwU/edit?usp=sharing

# Introduction
The workshop is about how to build web front-ends and remain sane. It focuses on core concepts in front-end web development, and topics such as modular code, web components, application state, data bindings etc. The emphasis is on understanding how things really work, rather than just providing simple recipes.

It's commonly known that front-end development ecosystem is incredibly fragmented, complicated, opinionated, rapidly changing and downright messy. While chasing another fashion, sound knowledge is easily forgotten, in favour of quick to-do recipes and StackOverflow-Driven-Development.

We aim to bring sanity to this. Truth is, all frameworks, methodologies and tools share similar core concepts and ideas. No-nonsense programming practices still apply, no matter what framework you're currently in love with. Our noble goal is to show you that there's beautiful structure beyond all that noise. Once you grasp it, you'll find yourself at ease even if you need to switch that framework again.

Ultimately, we won't be enforcing any specific framework or methodology. You can learn Angular or React elsewhere, there are plenty good tutorials and courses out there, and we will provide them as well. But right now, we want to show you HOW things work, not just WHAT TO DO to make something work. So be prepared for a good amount of vanilla JavaScript, HTML5 and CSS!

# The Workshop
The workshop will take 5 hours in total, divided in two sessions separated by a lunch break. There will be a lot of code and plenty of room for discussion. All the code presented during the workshop will be made available, so you can study and explore it later.

# The Topics

0. Intro : front-end development realm - choices, decisions, paradigms
1. Separation of concerns
2. How to name things in HTML and CSS
3. Web Components - Don't-Repeat-Yourself
4. Data bindings - from model to view and back
6. Application State - the single source of truth
5. Web Frameworks - discussion about how to choose one and why you'll be regretting it anyway

# The Audience
It would be nice if you bring in prior knowledge of front-end web development domain. You don't have to be an experienced programmer but acquaintance with HTML, client-side JavaScript and CSS is recommended. If you're not a programmer, you might still find the course entertaining. Just don't expect that we will be spending time on explaining the very basics, that's all.

# About Me
I'm Tomasz Waraksa, die-hard programmer from Poland, with 20+ years professional experience in software development. Starting with 8-bit computers, I went all the way through IT revolution until now, yet somehow managed to remain sane :-) I've been working all over Europe as independent consultant, architect, developer, team coach and mentor on many projects for customers of all kinds - startups, medium-sized businesses and the ones like Bank of America, Qualcomm or MasterCard. I speak 4 human languages (French comes next) and 10 programming languages, all on daily basis. Currently based in Dublin, Ireland, where I'm living, cycling, swimming, photographing, hiking, playing guitar, going to gigs and occasionally contributing to the noble cause of Hackers & Founders Meetup group.

