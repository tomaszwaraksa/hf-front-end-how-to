"If you really knew what has been done
 and tried by other people, you'd realise
 that nothing is terribly new"

 Saul Leiter, photographer