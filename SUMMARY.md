# Some Final Remarks
# Slide 40
# Slide 41

## A symptomatic question from your feedback: Is BEM still a thing?
Yes it is. Is assembler still a thing? You bet.

## Using state-of-the-art practices was a bit too much for me
And it always is. You must not use a framework, a library, a tool just for the sake of being state-of-the-art, period! It's obvious what I say, but please select the right tool for the right task and don't worry about the future, because:

- with or without state-of-the-art frameworks, you will be refactoring your code, as your app grows
- it's not frameworks that help in this progress, but:
    - proper code structure
    - logical and consistent naming conventions
    - using widely recognised industry standards, not the fashion of the day


## Module Loaders
In many of our web apps we just don't need module loaders. It's frameworks that impose them on us.

Using modular programming does not necessarily require using a module loader. Only if you have issues with complicated module interdependence, you might need a module loader. But often this is caused by convoluted code dependencies and wrong application design. Many projects have complicated loading infrastructure they just don’t need.

Bundling tools obfuscate your application, make development complicated, and last-but-not-least make debugging a horror, period. They're an important factor responsible for **but it works on my machine** syndrome. Yes, it does, as on dev machine you don't bundle.

Many of the benefits of loaders are just that – benefits – and not requirements. Try running your project without a loader at first. You’ll have greater control over and insight into your code. You mind find that you're happy without them, while adding it later is not hard.

Inspired by "The Short History of Module Loaders" [https://appendto.com/2016/06/the-short-history-of-javascript-module-loaders]()

If you're able to comprehend documentation of webpack, you're a robot. Here's an example:

>Since Loaders only execute transforms on a per-file basis,
>plugins are most commonly used (but not limited to) performing
>actions and custom functionality on "compilations" or "chunks"
>of your bundled modules (and so much more).

For Christ's sake. Dutch sounds better. And so much more.

Finally, remember:
- We're no longer in year 2008 with 2 parallel HTTP connections limit
- Browsers have caching, and it greatly reduce traffic, without **any** effort on your side! Frankly, in most cases it's your bloated code that causes slowness, not the download process
- HTTP/2 is coming and it further reduces need for bundling


## Things you could and should know and use
# Slide 42
Having ranted for so long, there are quite a few things beyond vanilla JS, that you could and should start using, as they might help you greatly:

* **NodeJS and npm** - for easy access to libraries and frameworks, for daily scripting and automation, for running those tiny local mock servers, and finally for publishing your own code and making your apps easily updatable

* **ES6/7** - the language brings in so many benefits that there's just no excuse to not to use it.

* **LESS** or **SASS** to overcome drastic limitations of CSS language. Again, plugins in IDEs will compile them for you on the fly, if you don't like / need build tools, like **Easy LESS plugin** in VSCode

* **Utility libraries** - some of them are indisputable, so embrace. My Swiss-army knife consists of: **JQuery** (!), **Lodash**, **Moment**, **Normalize.css** and **Animate.css**

* **Minimalistic frameworks** that grow with your app. AngularJS was an example of that, remember the minimalistic AngularJS app? Vue.js seems to be a good choice. And I don't mean React, where you can supposedly replace everything with everything else, and in the end you don't know what's your name. Look at [http://aurelia.io/]() made by the father of AngularJS who departed because of Angular 2 mind-boggling complexity. Look at [http://riotjs.com]() - a wonderfully simple library for building components, which you CAN use without any server-side build pipeline, THEN evolve into it as your project grows. There are rainbows and unicorns outside React and Angular.

* Adapt some popular UI design guidelines for **commodity** apps: **Bootstrap**, **Material Design**, really anything whatever that makes your app feel consistent inside and on the outside. Looking for something less bloated?[http://blazecss.com]() will do.

* Learn and aim for separation of concerns in your code, do your study. **GoF** book, although not the easiest, is highly recommended and will add much more to your skillset than any of the frameworks of the day.

## ES6/7
## Slide 43
The language brings in so many benefits that there's just no excuse to not to use it. If you don't like introducing complex build pipelines just to compile your ES6 code, consider **TypeScript** and **Visual Studio Code**, or plugin for IntelliJ / WebStorm. They compile your ES6 code as you type, without any effort or using build tools. The benefits are just priceless:
- native modules and namespaces
- *this* finally works like in other languages
- *let* won't fail you, like *var* does
- *async* instead of callback and promises, which aren't simpler than callbacks at all
- *const*
- *class* - as not everything is a function, really
- lambda expressions - as not everything is a function, really
- optional type checks


# References
## Slide 44
Code: https://bitbucket.org/tomaszwaraksa/hf-front-end-how-to.git

Slides: http://bit.ly/hf-frontend-howto

H&F Meetup Group: https://www.meetup.com/Hackers-and-Founders-Dublin
