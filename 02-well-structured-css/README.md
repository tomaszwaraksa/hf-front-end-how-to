# Introduction

## Slide 16

A well designed UI will have:

1. Decent visual hierarchy of meaningful elements
2. HTML markup reflecting this hierarchy
3. CSS classes reflecting this hierarchy
4. Meaningful names of elements

With separation of concerns in mind, one wants to have a good way for structuring and naming elements in his HTML and CSS code.

As a curiosity, recommendation #2 is **THE** reason why you shouldn't use <TABLE> tag. When a human looks at a page, he/she doesn't think about tables and grids and columns, but sees elements such as message list, login form with labels and fields neatly aligned etc. Using tags only for the sake of forcing a specific layout should be avoided if possible, and <TABLE> is the first one to go.

## Slide 17

I teach BEM. Of sorts :-)

Why BEM over the others?
No matter what methodology you choose to use in your projects, you will benefit from the advantages of more structured CSS and UI.
So I chose BEM, you can pick something else, and the principles are really going to be similar.

# Basic conventions
## Slide 18
Regardless of BEM, you should make agreement on basic rules, such as character casing, white space etc. It seems to be a widespread practice in front-end development to follow these rules:

1. Use only lower case for naming elements, to avoid mysterious errors due to case sensitivity of DOM engines
2. Separate words with single dashes -

Those simple rules greatly reduce possibilities for errors. No uppercase - no confusion whether I should look for buttonSave or ButtonSave. Only dash as separator - and there's no more confusion whether it was button_save or buttonsave or maybe buttonSave?

As usual, ANY set of well-defined rules is just as good - as long as it's consistent across the whole code base. Nevertheless, the above is recommended, as you will find yourself at home with thousands other front-end developers and their code.

# What is BEM
## Slide 19
BEM is naming convention for CSS classes, which stands for:
- Block
- Element
- Modifier

## Block
### Slide 20
Block is a standalone entity in your UI, which is meaningful on its own, within its broadly understood container. Think of menu, page footer, login form. Notice, that also simple elements such as button or checkbox fit this definition - they are standalone entities, meaningful on their own!

## Element
### Slide 21
It's a part of the block which has no standalone meaning. Taken out of context, it can't be reused or put into productive use. Think of entities such as menu item, button label, footer text, page header logo.

Sometimes element is complex enough to be a composite built of other, simpler elements.

## Modifier
### Slide 22
Modifier describes a state of a block or element, for example focused, checked, deleted, selected, important, big, emphasized etc.

Sometimes states can have multiple values, for example message element can have state tagged, with values such as critical, important, normal and ignored.

## Naming rules
1. For naming elements always apply the above basic conventions. In our case we decided on lowercase only, dash as word separator
2. CSS identifier or element or modifier is always prefixed with its predecessor, to avoid naming conflicts with other blocks and elements.

# Practical example - a message view
### Slide 23
### Code ./02-well-structured-css

A UI component displaying a list of messages. Messages can be read, replied, forwarded, tagged and deleted.

We can easily identify the following BEM hierarchy:

## Block
We have the following standalone blocks within this UI:

* messages
* message
* button

## Elements
The following elements hierarchy is proposed:

* messages
    * header
    * list
    * footer

* message
    * text
    * title
    * actions

* button
    * text
    * icon
        * icon a
        * icon b
        * icon c

## Modifiers
The following modifiers are proposed for some of the blocks and elements above:

* message
    * tag: important|spam
    * state: read|unread|deleted

* message actions
    * visible

* button
    * dangerous

## CSS
Following the above, we come up with the following CSS hierarchy:

    .messages { }
    .messages-header { }
    .messages-list { }
    .messages-footer { }

    .message {}
    .message-text {}
    .message-title {}
    .message-actions {}
    .message-tag-important {}
    .message-tag-spam {}
    .message-state-read {}
    .message-state-unread {}
    .message-state-deleted {}

    .button {}
    .button-type-dangerous {}