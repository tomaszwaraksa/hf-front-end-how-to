# Welcome
**5m**
- Welcome, thanks
- Beginning of the series, so there won't be a holy grail discovered today
- Variety of requests through feedback - Angular, React, build tools, testing, debugging, basics of HTML, CSS. Impossible to satisfy in one session and it't not what we meant anyway. We will provide product-specific trainings in future. Today it's about the deeper picture. We're not going to learn how to push buttons today, we'll rather learn how the buttons work!
- Our plan is to continue providing similar sessions. Some of them dedicated to very basics. Some very-product specific. Some, like today, about the principles and ideas.

# Separation of concerns
**10m**
A quick peek, in order to prepare you mentally to code structure in further topics.

# Well-structured CSS
**15m**

# Web Components
**1h30m**

# LUNCH BREAK
**1h**

# Data Bindings
**1h**

# Application State
**1h**

# Summary
**10min**

# Free talk about frameworks
**30m - 1h**