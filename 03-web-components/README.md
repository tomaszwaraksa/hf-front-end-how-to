# Introduction
## Slide 24
Main reason why programming languages evolve and frameworks come, is attempt to eliminate repeating yourself over and over. Repetition is a waste of time, disk space and money. It can also lead to inconsistency and errors (why?). Programmers love acronyms, and they've come up with those two for such occasions:

    DRY - Don't Repeat Yourself

and another, more straightforward

    DIE - Duplication is Evil

BTW, whoever can tell me in 1.2.3. what S.O.L.I.D. means, is a hero.

In any non-trivial application there will be a lot of moments when you would reuse the same piece of code, the same UI widget, the same functionality.

So, to make life easier, you want to decompose your application into a set of reusable components, primitive bricks that you can assemble into something more useful.

# Semantic markup
The choice of HTML elements, or tags, is pretty crude. DIV is the workhorse, few others for forms and fields, that's about it. There was time back in 90s when HTML was much richer. We've had <marquee> and <center> and even <menu>, and you still probably use <b> and <i> although everyone tells you to not to do so.

This trend was later reversed, as it was supposedly violation of separation of concerns. Tags responsible for layout would be mixed with tags responsible for fonts, with some other tags being actual functional components. HTML5 came, table was banned. But the idea of **Semantic Markup** was born. The idea is simple: tags should convey some meaning, not just serve as wrappers without any clear purpose (hey, DIV!).

# Your own HTML tags
The result is even a bigger mess, because DIV is here to stay while things like header, footer, aside, details, nav and whole lotta of them came: [https://www.w3schools.com/html/html5_new_elements.asp](https://www.w3schools.com/html/html5_new_elements.asp)

But the change of mindset is important, nevertheless. HTML markup would read much easier, if we could use components with meaningful names. They just don't have to be globally meaningful names. This is where HTML5 tag proliferation goes wrong. What if we, as developers, could devise our own tags, meaningful for us and our applications only, so W3C isn't bombarded anymore with reqeuests to include tag <GRANDPA> because it would be so good for fostering family relationships.

# A component is not just markup
## Slide 25
A component, in a sense like we're used from desktop application, is not just the way its structured, that would be HTML markup. It has looks, expressed in CSS. It has behaviour, expressed in JavaScript. Trouble is, how to marry these three **DIFFERENT PROGRAMMING LANGUAGES** into one coherent entity called component?

W3C tries since 5 years to come up with a sensible proposal for Web Components and they fail miserably. See (https://www.webcomponents.org)[https://www.webcomponents.org]. They're now onto v.2, and it's still not ready. Polymer framework is an attempt to utilize and tame this technology, already partially available in browsers. It's a flawed and quite difficult standard, with its own semi-religious terminology and off-putting syntax. And it's way too late in the game.

This is where frameworks arrived, to fill in the obvious gap. Practically all of them offer a solution to the above challenge. Let's see how frameworks make life better with components.

[01 real-life-example/angularjs/index.html](AngularJS)
[01 real-life-example/riot/index.html](Riot)
[01 real-life-example/vue/index.html](Vue)

# HTML limitations and Single Page Application model
Before we talk more about components, let's introduce **SPA**, as it heavily relies on components.

## Slide 26
The root problem with HTML is that it's never been designed with reusability in mind. Inherently, it's a stateless protocol for viewing text documents, not a framework for building feature-rich web applications. But, with help of JavaScript, we can mitigate that.

Main issue with HTML is that every single page in your application has to repeat everything over and over again. Menus, footers, dialogs etc. Navigate to another page - and all these elements are fetched from the server again.

This has been mitigated with rise of server-side application frameworks like Ruby on Rails or ASP.NET. From programmer's perspective, there was at last some reusability of code. But, once the page has been rendered by a server-side engine, the browser would still receive the whole layout with each and every page, over and over again.

Historically, there was no such thing in HTML as web component. If you wanted to reuse certain UI elements, the only native HTML way of doing that was by using frames. A real heavyweight, as pages using frames load slower, render slower, and in general just feel clumsy.

One would think: why not get rid of the problem altogether by keeping the user on one page, all the time? If all information he requires is there, a few links and a bit of scrolling would do, right?

# Simplest single-page application ever
[02 spa-explained/spa-01.html](02 spa-explained/spa-01.html)

Works nice :-)

## Single-page application, a bit smarter
A little problem is though with the fact, that all data is visible there, and it doesn't look good this way - if you just scroll down. With a bit of JavaScript we can mitigate that.

[02 spa-explained/spa-02.html](02 spa-explained/spa-02.html)

## Fixing problem with manual URL entry
We're getting somewhere. No ugly scrolling, articles are revealed only when requested by a click on the menu. There are still two issues with the previous solution:

* URL indicates which article we're looking at - article name is after hash tag. But if I enter that URL manually, nothing happens.
* The page still loads ALL articles at once. If there were hundreds of them, but the user will only read one or two, this is a major waste of computing power. We can't afford that.

Let's try fixing the first problem:

[02 spa-explained/spa-03.html](02 spa-explained/spa-03.html)

Nice! Click on the menu item shows the article, but entering the URL just as well. Navigating our page is now consistent and SEO-friendlier.

## Fixing problem with loading all content at once
The last challenge is to load those articles only when they're actually needed. Once we have that, we've prepared ground for real web components. And let's add some caching as well, it's not that difficult at all!

[02 spa-explained/spa-04.html](02 spa-explained/spa-04.html)


# Simple Web Components

## Slide 27
## Slide 28
## Slide 29
## Slide 30

More specifically, we want:

- application made of fully reusable components
- semantic markup rather than divs with CSS classes
- components loaded only when really used on page
- components able to receive input parameters and use them to dynamically render their markup

## Sample code: [03 simple-components/index.html](03 simple-components/index.html)

## Slide 31
All this with a library of mere 71 lines of code, of which 1/3 is whitespace, comment lines and curly brackets.

