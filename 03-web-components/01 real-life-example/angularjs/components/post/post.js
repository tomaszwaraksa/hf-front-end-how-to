'use strict';

// Register post component, represented by <hf-post> tag
angular.module("app").component("hfPost", {
    // parameters a.k.a. props passed with the tag
    bindings: {
        title: "@",
        text: "@",
    },

    // component template
    templateUrl: "components/post/post.html",

    // component controller
    controller: function() {
        this.$onInit = function() {
            console.log("hf-post loaded");
        }
    }
});