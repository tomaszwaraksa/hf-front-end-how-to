'use strict';

// Register header component, represented by <hf-header> tag
angular.module("app").component("hfHeader", {
    // component template
    template: "<div class='header'>AngularJS Components by Hackers and Founders Meetup</div>",

    // component controller
    controller: function() {
        this.$onInit = function() {
            console.log("hf-header loaded");
        }
    }
});