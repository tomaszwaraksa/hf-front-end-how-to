'use strict';

var App = {
    // Initializes the application
    initialize: function()  {
        riot.mount("*");
    }
}
