'use strict';

// Register post component, represented by <hf-post> tag
Vue.component("hf-post", {
    props: ["title", "text"],
    template: "<div class='post'><div class='post-title'>{{title}}</div><div class='post-text'>{{text}}</div></div>"
});

