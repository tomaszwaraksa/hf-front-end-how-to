'use strict';

// Register header component, represented by <hf-header> tag
Vue.component("hf-header", {
    // component template
    template: "<div class='header'>Vue Components by Hackers and Founders Meetup</div>"
});
