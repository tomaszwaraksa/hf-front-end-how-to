'use strict';

var App = {
    // Vue application handle
    instance: null,

    // Initializes Vue application
    initialize: function()  {
        App.instance = new Vue({
            el: "#app"
        });
    }
}
