'use strict';

const Application = { 
    // Registered components
    components: {},

    // Registers a component
    registerComponent: function(name, options) {
        var component = new Component(name, options);
        if (component) {
            this.components[component.name] = component;
        }
        console.log("Component registered:", name);
    },

    // Finds a component with the specified name
    getComponent: function(name) {
        return this.components[name];
    },

    // Renders all components on the page
    load: function() {
        for (var name in this.components) {
            // get component
            var component = Application.getComponent(name);
            // find all its instances on the page
            var containers = document.querySelectorAll(component.tag);
            // replace with component markup
            containers.forEach(function(container) {    
                component.load(container);
                console.log("Component loaded:", component.name);
            });
        }
    }
};
