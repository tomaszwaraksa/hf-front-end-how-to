'use strict';

// Component for retrieving posts from the server
var PostService = {
    // Loads post with the specified ID, returns promise
    get: function(id) {
        var url = "posts/" + id + ".json";
        return fetch(url)
            .then(function(response) {
                return response.json();
            });
    }
};