'use strict';

Application.registerComponent({
    name: "post",
    tag: "hf-post",
    templateUrl: "app/components/post/post.html",

    onLoad: function(container) {
        // Get identifier of the post to load, specified as id="" tag of <hf-post> component
        var postId = container.getAttribute("id");
        console.log("Loading post: ", postId);
        // Fetch the post
        if (postId) {
            PostService
                .get(postId)
                .then(function(post) {
                    // Insert title into placeholder
                    var title = container.querySelector(".post-title");
                    if (title) {
                        title.innerHTML = post.title;
                    }
                    // Insert text into placeholder
                    var text = container.querySelector(".post-text");
                    if (text) {
                        text.innerHTML = post.text;
                    }
                });
        }
    }
});
