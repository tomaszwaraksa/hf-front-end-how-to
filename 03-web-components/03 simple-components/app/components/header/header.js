'use strict';

Application.registerComponent({
    name: "header",
    tag: "hf-header",
    template: "<h1>{{text}}</h1>"
});