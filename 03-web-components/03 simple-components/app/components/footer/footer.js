'use strict';

Application.registerComponent({
    name: "footer",
    tag: "hf-footer",
    template: "<div class='footer'>(c) Hackers &amp; Founders, Dublin, 2017</div>"
});