'use strict';

Application.registerComponent({
    name: "menu",
    tag: "hf-menu",
    templateUrl: "app/components/menu/menu.html",

    onLoad: function(container) {
        console.log("Menu ready to go!");
    }
});