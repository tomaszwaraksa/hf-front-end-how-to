'use strict';

/** Creates a component
 *
 *  Options:
 *      name: human-friendly name of the component
 *      tag: HTML tag representing the component
 *      template: component markup
 *      templateURL: HTML file with component markup, alternative to template
 *      onLoad: callback executed when component has been injected into the page
 */
function Component(options) {
    var component = this;

    // Inject options
    Object.assign(component, options);

    // If no tag specified, use lowercased name
    if (!component.tag) {
        component.tag = options.name.toLowerCase();
    }
}

/** Processes component markup, by resolving references to attributes.
 *  In other words, very simple data binding!
 *  I know, I know, this can be done so much better,
 *  I recommend you look up John Resig's Micro-Templating 140 bytes
 *  bad-ass piece of JavaScript with variables, loops and OMG
 *  https://gist.github.com/Fedia/20d41d8533e0903f0123
 */
Component.prototype.databind = function(container, markup) {
    for (var i=0; i < container.attributes.length; i++) {
        var attr = container.attributes[i];
        var expression = "{{" + attr.name + "}}";
        markup = markup.replace(expression, attr.value);
    }
    return markup;
}

/** Loads the component into the specified container */
Component.prototype.load = function(container) {
    var component = this;

    // If template markup is specified, just inject into container
    if (component.template) {
        // Otherwise just inject component markup into the container
        container.innerHTML = component.databind(container, component.template);
        // initialize the component
        if (component.onLoad) {
            component.onLoad(container);
        }
    }
    // If template URL is specified, markup needs to be fetched
    else if (component.templateUrl) {
        fetch(component.templateUrl)
            .then(function(response) {
                return response.text();
            })
            .then(function(text) {
                // inject into the container
                container.innerHTML = component.databind(container, text);
                // initialize the component
                if (component.onLoad) {
                    component.onLoad(container);
                }
            });
    }
    else {
        // Otherwise signal error
        container.innerHTML = "<div>YOU FORGOT YOUR TEMPLATE, MISTER!</div>";
    }
}
