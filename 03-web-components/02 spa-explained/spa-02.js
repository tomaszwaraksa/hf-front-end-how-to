var Articles = {
    current: null,

    load: function(id) {
        // hide previous article
        if (Articles.current) {
            Articles.current.className = "article article-hidden";
            Articles.current = null;
        }
        // find container with the article to load
        var element = document.querySelector("#" + id);
        if (element) {
            // reveal it
            Articles.current = element;
            Articles.current.className = "article article-visible";
        }
    }
};
