var Articles = {
    current: null,

    initialize: function() {
        // whenever hash part of the URL changes, let us know
        window.addEventListener("hashchange", function() {
            var articleId = location.hash;
            if (articleId) {
                // location contains hash character, so get rid of it, we just want ID
                articleId = articleId.substring(1, articleId.length);
                console.log("Navigating to", articleId);
                Articles.load(articleId);
            }
        });
    },

    load: function(id) {
        // hide previous article
        if (Articles.current) {
            Articles.current.className = "article article-hidden";
            Articles.current = null;
        }
        // find container with the article to load
        var element = document.querySelector("#" + id);
        if (element) {
            // reveal it
            Articles.current = element;
            Articles.current.className = "article article-visible";
        }
    }
};
