var Articles = {
    container: null,

    // Initialize the navigation system
    initialize: function() {
        // Find the article container
        Articles.container = document.querySelector(".article");

        // whenever hash part of the URL changes, let us know
        window.addEventListener("hashchange", function() {
            var articleId = location.hash;
            if (articleId) {
                // location contains hash character, so get rid of it
                articleId = articleId.substring(1, articleId.length);
                console.log("Navigating to", articleId);
                Articles.load(articleId);
            }
        });
    },

    // Load article with the specified ID
    load: function(id) {
        // hide previous article
        Articles.container.innerHTML = "";

        // fetch article content from server
        var url = "articles/" + id + ".html";
        console.log("Fetching article from ", url);
        fetch(url)
            .then(function(response) {
                return response.text();
            })
            .then(function(text) {
                console.log("Fetched the article", text);
                Articles.container.innerHTML = text;
            });
    }
};
