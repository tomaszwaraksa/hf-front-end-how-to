# Front-End Development - The State of Madness

Recommended reading:
[http://bit.ly/learn-js-in-2016](How it feels to learn JavaScript in 2016)

## Slide 3
The slide shows some of the web front-end development technologies, platforms and tools in circulation. I'm pretty sure that many of the items are already deemed obsolete, while many hot newcomers aren't even mentioned.

Proliferation of frameworks beyond comprehension. Proliferation of tools just for the sake of it. Recent example - Google promoting **yarn** as replacement for **npm**. After all the media buzz I still have no clue what exactly are they trying to fix that's wrong with npm. The only viable reason seems to be just another attempt at vendor lock, but with their weight, they might just pull it off again.

## Slide 4
The best way to make you shine, is to adopt new incomprehensible terminology and elaborate rituals. Try angular-cli for bootstrapping your first Angular 2/3/4 app. A number of magic chants which only a few truly understand:

npm install -g @angular/cli
ng new hello-world
cd hello-world
ng serve

navigate to http://localhost:4200/

# Slide 5
WTF has just happened? Has my PC just turned into AI and is about to take over the world? A whole new civilisation just came to existence in my humble hello-world folder, which now contains:

52,700 items
202,407,127 bytes

All this for what? For an app that displays Hello World.

## Slide 6
... suspension

## Slide 7
Now let's do that in python:

$ python
>>> print "Hello, world!"

## Slide 8
0 bytes
0 files

## Slide 9
Obviously, you don't need all that for a Hello World app. And it is possible to strip the folder of most of the bloat (although you can be pretty sure that you'll be still left with hundreds of files). Problem is elsewhere: the makers of the framework make this madness default. It comes out of the box like that and we only learn, painfully and slowly, to manage it. While good framework is the one that grows only as your needs and your understanding grows. Angular 1 was a much better framework in that aspect.

## Slide 10
What to do about it?

- Stop chasing it.
- By doing so you contribute to the problem.
- Focus on the principles and you’ll survive them all.

It really can be done simpler. What was wrong with this?
## Type the code for hello-world-angular1
* 15 lines of code (only because we're nice and like neatly formatted code).
* it has modules
* it has custom components
* it has data bindings
* 5 lines more and you have application state

## Slide 11
During this and subsequent courses we're going to focus on understanding how things work. We will introduce principles and concerns, shared by every developer. We will try addressing them with just what's available in the browser. We'll try to see how framework help doing the same. But most of all, we'll try to understand them!