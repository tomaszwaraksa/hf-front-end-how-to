// This declares that there's a module called HelloWorld in our app
angular
    .module("HelloWorld", []);

// When the module kicks in, this will be called once.
// Actually, this is not required unless you do have some initialization code
// to run. We have it here for demonstration purposes only, to show the
// lifetime cycle of our application
angular
    .module("HelloWorld")
    .run(function() {
        console.log("It's alive!");
    });