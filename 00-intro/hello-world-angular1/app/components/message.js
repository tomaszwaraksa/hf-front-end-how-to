// This declares a component represented by markup <hf-message text=""></hf-message>
angular
    .module("HelloWorld")
    .component("hfMessage", {
        // parameters of the component
        bindings: { text: "@" },

        // internal markup of the component
        template: "<h1>{{$ctrl.text}}</h1>",

        // internal logic of the component
        controller: function() {
            console.log("hf-message at your service!");
        }
    });