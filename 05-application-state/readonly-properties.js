'use strict';

const JS = require("./readonly-properties-js.js");
const ES = require("./readonly-properties-es.js");
const TS = require("./readonly-properties-ts.js");

var State = JS.State;

// Get state
var state = State.get();
console.log("GOT STATE!");
// Play with menu visibility
console.log("1. Menu visibility:", state.menuVisible);
state.hideMenu();
console.log("2. Menu visibility:", state.menuVisible);
// Attempt to change readonly property
try {
    state.menuVisible = true;
}
catch (e) {
    console.log("3. Assignment error:", e.message);
}
// Play with user
console.log("4. Current user:", state.currentUser);
state.login({ name: "tomasz"});
console.log("5. Current user:", state.currentUser);

// Get state again - we should see the same instance
 state = State.get();
 console.log("GOT STATE!");
 console.log("1. Menu visibility:", state.menuVisible);
 console.log("2. Current user:", state.currentUser);