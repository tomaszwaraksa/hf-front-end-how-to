# Application State - The Single Source of Truth
## Slide 40

## What is application state?
Applications keeps plenty of data IN MEMORY, in order to perform their functions. Things such as: logged in user, all application users, customers, an invoice that's being now entered, products, positions and visibility of menus or popup dialogs,data required for undo operation, browsing history, list filters etc. etc.

Which ones are application state and which not? It seems pretty hard to decide.

React/Flux has therefore taken a simple stance - everything is application state. Well, nice try, though this is probably the single reason why Flux is so hard to grasp, and applications notoriously hard to digest once they grow.

Here's another view:
## Slide 41
>*Application data*
>Information kept and processed by the application
>to fulfill its business purpose

>*Application state*
>Information kept and processed by the application
>to execute its runtime logic

Still pretty unclear, right? How about another definition then? Imagine that your application abruptly goes down. In this particular event:

## Slide 42
>*Application data*
>Everything that you cannot afford to lose when things break down

>*Application state*
>Everything else that you don't care about in such drastic event

A good analogy is a truck with payload. Application state is whereabouts of the truck - its speed, fuel level, geoposition, distance from target, tyre pressure etc. Application data - is the payload. I want to have reliable current information about the former, but I don't care much about this information a minute later. A the same time, I want to keep the payload safe and secure no matter what.

## Slide 43
Now the division becomes very clear. We can now easily split the above sample:

Application data:

* users
* customers
* invoices
* products

Application state:

* user session
* the state of menus or popup dialogs
* undo history
* browsing history
* list filters

Vague cases:

* shopping basket

## What to do about it?
## Slide 44
- Keep state separated from application data
- Keep data entities free of transitional state
- Use singleton state
- Use functions for accessing and modifying state

### Example - logged in user
This is clearly information that you want to keep accessible as a singleton object, shared by all the other components of your application. They can poll it easily and disable or enable their functionality accordingly.

## How to do it?
Just simple. Define a singleton JavaScript object and make all other code use it. In Angular that would be a service, which by definition is a singleton. With pure JavaScript you'd need to resort to read-only properties to make things safe.

Rather use functions to access the values, than access object properties directly. This makes it easier to change internal structure of state object without ripples across the whole app. React/Flux principle - modify state only by actions, makes a lot of sense in complex applications. This gives you certain freedom in shaping the internals of your state, without impacting rest of your app. This also allows easy testing and mocking. And it's possible (though not trivial) to create readonly properties in JavaScript. Much easier in ES6.
## Slide 45

# Objective
- Three forms: order quantity and billing address
- User navigates between forms at will
- Data is preserved as we navigate between forms

# How about frameworks?
Every one of them offers a different solution. Angular's proposal are shared services, which are guaranteed to be singletons. React/Flux way is by making state inherently immutable, modified only by actions.

## AngularJS


