'use strict';

// State singleton
export class State {
    private static instance;
    private _menuVisible = true;
    private _currentUser = null;

    constructor() {
        if (!State.instance) {
            State.instance = this;
        }
        return State.instance;
    }

    // Gets singleton instance of the state
    static get() {
        if (!State.instance)
            State.instance = new State();
        return State.instance;
    }

    // Readonly properties
    get menuVisible() {
        return this._menuVisible;
    }

    get currentUser() {
        return this._currentUser;
    }

    // State modifiers
    showMenu() {
        this._menuVisible = true;
    };

    hideMenu() {
        this._menuVisible = false;
    };

    login(user) {
        this._currentUser = user;
    };

}
