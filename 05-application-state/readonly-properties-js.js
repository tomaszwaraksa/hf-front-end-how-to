'use strict';

// State singleton, make it public
module.exports.State = (function() {
    function StateInstance() {
        var _menuVisible = true;
        var _currentUser = null;

        // Readonly properties
        Object.defineProperty(this, "currentUser", {
            get: function() {
                return _currentUser;
            }
        });

        Object.defineProperty(this, "menuVisible", {
            get: function() {
                return _menuVisible;
            }
        });

        // State modifiers
        this.showMenu = function() {
            _menuVisible = true;
        };

        this.hideMenu = function() {
            _menuVisible = false;
        };

        this.login = function(user) {
            _currentUser = user;
        };
    }

    var state;
    return {
        get: function() {
            if (!state) {
                state = new StateInstance();
            }
            return state;
        }
    }
})();

