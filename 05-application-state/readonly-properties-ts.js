'use strict';
// State singleton
var State = (function () {
    function State() {
        this._menuVisible = true;
        this._currentUser = null;
        if (!State.instance) {
            State.instance = this;
        }
        return State.instance;
    }
    // Gets singleton instance of the state
    State.get = function () {
        if (!State.instance)
            State.instance = new State();
        return State.instance;
    };
    Object.defineProperty(State.prototype, "menuVisible", {
        // Readonly properties
        get: function () {
            return this._menuVisible;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(State.prototype, "currentUser", {
        get: function () {
            return this._currentUser;
        },
        enumerable: true,
        configurable: true
    });
    // State modifiers
    State.prototype.showMenu = function () {
        this._menuVisible = true;
    };
    ;
    State.prototype.hideMenu = function () {
        this._menuVisible = false;
    };
    ;
    State.prototype.login = function (user) {
        this._currentUser = user;
    };
    ;
    return State;
}());
exports.State = State;
