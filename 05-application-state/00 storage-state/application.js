var Application = {

    // Application state
    state: {
        view: null,
        menuVisible: true,
        billing: Address.billing,
        shipping: Address.shipping,
        order: Order
    },

    // Initializes the app
    initialize: function(view) {
        // Restore the state
        Application.loadState();
        // determine which data to edit, by checking query string
        Application.state.view = view;
        var data = Application.getData(view);
        DataBinder.apply(data);
        console.log("Data loaded", data);
    },

    // returns the data to edit
    getData: function(type) {
        return Application.state[type];
    },

    // Navigates to the specified view
    goto: function(view) {
        // Save the state
        Application.saveState();
        // Change the view
        window.location = view + ".html";
    },

    // Saves the state to storage
    saveState: function() {
        window.localStorage.setItem("State", JSON.stringify(Application.state));
        console.log("Stored the state", Application.state);
    },

    // Loads the state from storage
    loadState: function() {
        var data = window.localStorage.getItem("State", data);
        if (data) {
            try {
                data = JSON.parse(data);
                Application.state = data;
                console.log("Restored the state", data);
            }
            catch (e) {
            }
        }
    }

};
