'use strict';

// Register header component, represented by <hf-summary> tag
angular.module("app").component("hfSummary", {
    // component template
    templateUrl: "components/summary/summary.html",

    // controller
    controller: function(State) {
        // constructor
        this.$onInit = function() {
            this.billing = State.billing;
            this.shipping = State.shipping;
            this.order = State.order;
        },

        // Saves the order
        this.save = function() {
            State.save();
            console.log("Order submitted", this.order);
        };

        // Cancels the order
        this.cancel = function() {
            console.log("Order cancelled");
            State.goto("billing");
        };
    }

});