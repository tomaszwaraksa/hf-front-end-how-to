'use strict';

// Register header component, represented by <hf-address-view> tag
angular.module("app").component("hfAddressView", {
    // Data bindings
    bindings: {
        // address to display
        address: "@"
    },

    // component template
    templateUrl: "components/address/address-view.html",

    // controller
    controller: function(State) {
        
        // gets address to view 
        this.getAddress = function() {
            if (this.address == "billing")
                return State.billing;
            if (this.address == "shipping")
                return State.shipping;
        },

        // Returns address text
        this.getAddressText = function() {
            var address = this.getAddress();
            if (address) {
                return address.street + ", " + address.postalCode + " " + address.city + ", " + address.country;
            }
        };
    }

});