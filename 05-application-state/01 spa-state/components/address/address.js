'use strict';

// Register header component, represented by <hf-address> tag
angular.module("app").component("hfAddress", {
    // Data bindings
    bindings: {
        // address to display
        address: "@"
    },

    // component template
    templateUrl: "components/address/address.html",

    // controller
    controller: function(State) {
        // gets address to edit 
        this.getAddress = function() {
            if (this.address == "billing")
                return State.billing;
            if (this.address == "shipping")
                return State.shipping;
        },

        // proceeds to the next step
        this.gotoNext = function() {
            State.gotoNext();
        };
    }

});