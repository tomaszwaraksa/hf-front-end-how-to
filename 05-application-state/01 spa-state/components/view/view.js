'use strict';

angular.module("app").component("hfView", {

    templateUrl: "components/view/view.html",

    controller: function(State) {        

        // returns true if specified view is now loaded
        this.isView = function(view) {
            return State.view.current == view;
        };
    }
});