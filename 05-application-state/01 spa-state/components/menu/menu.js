'use strict';

// Register header component, represented by <hf-menu> tag
angular.module("app").component("hfMenu", {
    // component template
    templateUrl: "components/menu/menu.html",

    // component controller
    controller: function(State) {

        // Menu items
        this.items = [
            {
                title: "Billing Address",
                view: "billing"
            },
            {
                title: "Shipping Address",
                view: "shipping"
            },
            {
                title: "Order Summary",
                view: "summary"
            }
        ];

        // returns true if specified view is now loaded
        this.isView = function(view) {
            return State.view.current == view;
        };

        // Navigates to the specified view
        this.goto = function(view) {
            State.goto(view);
        };

    }
});