// Registers a service called State,
// a singleton with application state
angular.module("app").factory("State", function(Address, Order) {

    var state = {
        // current view
        view: {
            // Current view
            current: null,
            // All available views
            all: ["billing", "shipping", "summary"]
        },
        // billing address
        billing: Address.billing,
        // shipping address
        shipping: Address.shipping,
        // Order summary
        order: Order,

        // Proceeds to the specified view
        goto: function(view) {
            // check if valid view
            if (state.view.all.indexOf(view) > -1) {
                state.view.current = view;
                console.log("View changed to:", view);
                return true;
            }
        },

        // proceeds to the next view in the process
        gotoNext: function() {
            var next = null;
            switch (state.view.current) {
                case "billing":
                    next = "shipping";
                    break;
                case "shipping":
                    next = "summary";
                    break;
            }
            return state.goto(next);
        }

    };

    return state;

});