// Registers a service called Address,
// a singleton with address data
angular.module("app").factory("Address", function() {
    var address = {

        empty : {
            description: "Select address ...",
            street: "",
            city: "",
            postalCode: "",
            country: "",
            isPrimary: false
        },
        
        shipping : {
            description: "Shipping Address",
            street: "Grafton Street",
            city: "Dublin",
            postalCode: "D1",
            country: "Ireland",
            isPrimary: true
        },
        
        billing: {
            description: "Billing Address",
            street: "Henry Street",
            city: "Belfast",
            postalCode: "BT9 5GR",
            country: "Northern Ireland",
            isPrimary: false
        }
    };

    return address;
});


