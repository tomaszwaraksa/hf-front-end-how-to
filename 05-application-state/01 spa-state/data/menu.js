'use strict';

// Register header component, represented by <hf-menu> tag
angular.module("app").component("hfMenu", {
    // component template
    templateUrl: "components/menu/menu.html"
});