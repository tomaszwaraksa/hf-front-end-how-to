// Registers a service called Order,
// a singleton with order data
angular.module("app").factory("Order", function() {

    var order = {
        id: "A/8871676",
        customer: "MasterCard",
        total: 130,
        currency: "USD"
    };

    return order;

});