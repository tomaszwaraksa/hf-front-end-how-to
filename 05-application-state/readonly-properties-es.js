'use strict';

// State singleton
const STATE = Symbol();
global[STATE] = null;

class State {

    constructor() {
        if (!global[STATE]) {
            // safely store state singleton
            global[STATE] = this;

            // prepare accessors for local fields
            this._menuVisible = Symbol();
            this._currentUser = Symbol();

            // set initial values
            this.showMenu();
            this.login(null);
        }
        return global[STATE];
    }

    // Gets singleton instance of the state
    static get() {
        if (!global[STATE])
            global[STATE] = new State();
        return global[STATE];
    }

    // Readonly properties
    get menuVisible() {
        return this[this._menuVisible];
    }

    get currentUser() {
        return this[this._currentUser];
    }

    // State modifiers
    showMenu() {
        this[this._menuVisible] = true;
    };

    hideMenu() {
        this[this._menuVisible] = false;
    };

    login(user) {
        this[this._currentUser] = user;
    };

}

// Make State class public
module.exports.State = State;