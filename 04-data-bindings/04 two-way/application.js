var Application = {
    
    // Currently displayed address
    address: Address.shipping,

    // Initializes the app
    initialize: function() {
        DataBinder.apply(Application.address);
    },

    // Saves the data
    save: function() {
        console.log("Address saved", Application.address);
    },



};
