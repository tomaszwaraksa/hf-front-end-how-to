# Data Bindings

## 01 Real-life example with KnockoutJS framework
Not the newest, but still very popular and probably the easiest to explain.

## 02 Simple hand-made data bindings
Just to explain the problem, we show how to transfer data from model to UI and back. No automatism here, but direction of data flow is clear. This helps settle the ground for more complex scenarios.

## 03 One-way data bindings
Automatic one-way data bindings from model to UI. The form displays an address. Whenever another address is assigned in JavaScript, input controls are automatically populated. In the example we demonstrate that by switching between shipping and billing address.

## 03 Two-way data bindings
Automatic two-way data bindings from model to UI and back, an extension of the above scenario. Simple but extensible library which allows binding multiple controls to the same property, not just inputs but also text blocks like div or span. The flow is then visible with naked eye:
 
 - change in the input propagates to model
 - model notifies other bound controls
 - all controls are up-to-date with the model