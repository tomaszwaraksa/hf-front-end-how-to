// Generic two-way data binder
var DataBinder = {

    // Collection of data bindings - keys are properties, values are arrays of bound controls
    bindings: { },

    // Add binding to the list of bindings
    addBinding: function(property, control) {
        if (!DataBinder.bindings[property])
            DataBinder.bindings[property] = [];
        DataBinder.bindings[property].push(control);
    },

    // Sets value of a control
    populateControl: function(control, value) {
        switch (control.tagName) {
            case "INPUT":
                if (control.value != value)
                    control.value = value || "";
                break;

            case "DIV":
            case "SPAN":
            case "H1":
            case "H2":
            case "H3":
                if (control.innerHTML != value)
                    control.innerHTML = value || "";
                break;
        }
    },

    // Sets up observer on the specified object property, updates the control when it changes
    observeProperty: function(o, property) {
        // inner field where property value is stored
        var store = "__" + property;
        // transfer current value into the inner field
        o[store] = o[property];
        // define property
        Object.defineProperty(o, property, {
            get: function() {
                return o[store];
            },
            set: function(value) {
                // Whenever property is changed, update all controls bound to it
                o[store] = value;
                DataBinder.bindings[property].forEach(function(control) {
                    DataBinder.populateControl(control, value);
                });
            }
        });
    },


    // Sets up observer on control's change event, triggers model update when it changes
    observeControl: function(o, property, control) {
        if (control.tagName == "INPUT") {
            control.addEventListener("input", function() {
                // if input value has changed, transfer it to the bound property
                if (o[property] != control.value) {
                    o[property] = control.value;
                }
            });
        }
    },

    // Set up data bindings for all data-bound inputs
    apply: function(data) {        
        data = data || {};
        document.querySelectorAll("*").forEach(function(control) {
            var property = control.getAttribute("data-bind");
            if (property) {
                // Add binding to the list
                DataBinder.addBinding(property, control);
                // set initial value
                DataBinder.populateControl(control, data[property] || "");
                // set up observers
                DataBinder.observeProperty(data, property);
                DataBinder.observeControl(data, property, control);
            }
        });
        console.log("Data bindings ready.");
    }

};