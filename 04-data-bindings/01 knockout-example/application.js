var Application = {

    // Currently displayed address.
    // Assign empty address, but wrapped in ko.observable.
    // When Knockout kicks in, it will keep an eye on any changes to Application.address.
    // Whenever address is changed, data bindings will be triggered and UI will update automagically.
    address: ko.observable(Address.empty),

    // Indicates whether payment has been completed
    paid: ko.observable(false),

    // Indicates that address has been selected
    addressSelected: ko.observable(false),

    // Initializes the app
    initialize: function() {
        // Knockout, go for it!
        ko.applyBindings(Application);
    },

    // Assigns an new address
    setAddress: function(address) {
        Application.paid(false);
        Application.addressSelected(true);
        Application.address(address);
        console.log("Address set:", Application.address());
    },

    // Submits the payment
    pay: function() {
        Application.paid(true);
        console.log("Paid!");
    }

};
