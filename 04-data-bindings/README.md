# Data Bindings
## Slide 32
T.B.D.

#MVC vs MVP Riddle
## Slide 33
## Slide 34

# Example of data bindings with Knockout JS
## Slide 35
T.B.D.

# H&F Data Bindings Library, Take 1 and 2
## Slide 36
## App: [02 simple/index.html](02 simple/index.html)

# H&F Data Bindings Library, Take 3
## Slide 37
## App: [03 one-way/index.html](03 one-way/index.html)

# H&F Data Bindings Library, Take 4
## Slide 38
## App: [04 two-way/index.html](04 two-way/index.html)

# Conclusions
## Slide 39
It's just-not-easy.

We only tackled simple property-to-value or property-to-innerHTML bindings. How to have expressions, loops, conditions, switch statements?

Separation of concerns is in grave danger. Sooner or later your HTML looks actually like imperative JS code, thinly disguised with handlebars.

Performance challenges which costed AngularJS the throne. How update a minimal possible set of DOM nodes or model properties?

Consistency challenges - with all that freedom of binding anything to anything, how to keep UI and model consistent, which brings us straight to the problem of **Application State**








