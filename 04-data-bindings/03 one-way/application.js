var Application = {

    // Currently displayed address
    address: Address.empty,


    // Initializes the app
    initialize: function() {
        Application.setAddress(Address.billing);
    },


    // Sets currently edited address
    setAddress: function(address) {
        Application.address = address;
        Application.dataBind();
        console.log("Address assigned", Application.address);
    },


    // Assigns postal code
    fillPostalCode: function() {
        Application.address.postalCode = "D18 V6V6";
        console.log("Postal code assigned", Application.address);
    },


    // Fills in databound controls with object properties
    updateUI: function() {
        document.querySelectorAll("input").forEach(function(input) {
            var property = input.getAttribute("data-bind");
            input.value = Application.address[property] || "";
        });
        console.log("UI updated");
    },


    // SET WATCH ON Application.address
    // GOOD - Any assignment to Application.data automatically triggers data bindings
    // BAD - it's still a one-way binding!
    // BAD - we're not observing changes on specific values, but that wouldn't be difficult to add :-)
    dataBind: function() {
        // Observe changes in address
        Application.observeProperty(Application, "address");
        // Observe changes in address properties as well
        if (!Application.address.observed) {
            for (var key in Application.address) {
                Application.observeProperty(Application.address, key);
            }
            Application.address.observed = true;
        }
        // Update the UI for the first time
        Application.updateUI();
    },


    // Sets observer on object property, which will trigger data bindings if the value changes
    observeProperty: function(o, property) {
        // Define a property with setter, which triggers data binding any time
        // we assign a new value to the property.
        // Values are stored in inner field starting with __
        var store = "__" + property;
        // Copy current value
        o[store] = o[property];
        // Create observed property
        Object.defineProperty(o, property, {
            get: function() {
                return o[store];
            },
            set: function(value) {
                if (o[store] != value) {
                    o[store] = value;
                    Application.updateUI();
                }
            }
        });
    }

};
