var Application = {

    // Currently displayed address
    address: Address.empty,

    // Initializes the app
    initialize: function() {
        Application.setAddress(Address.billing);
    },

    // Sets currently edited address
    setAddress: function(address) {
        Application.address = address;
        Application.dataBind1();
        //Application.dataBind2();
        console.log("Address assigned", Application.address);
    },

    // Assigns postal code
    fillPostalCode: function() {
        Application.address.postalCode = "D18 V6V6";
        console.log("Postal code assigned", Application.address);
    },

    // Various implementations of data bindings ===========================================
    // NAIVE
    // GOOD - just works
    // BAD - verbose, tight coupling between UI and data, JS code needs to be modified each time
    //       a new field is added to the form
    dataBind1: function() {
        document.querySelector("input[name='street']").value = Application.address.street || "";
        document.querySelector("input[name='city']").value = Application.address.city || "";
        document.querySelector("input[name='postalCode']").value = Application.address.postalCode || "";
        document.querySelector("input[name='country']").value = Application.address.country || "";
        console.log("dataBind1 executed");
    },

    // USE INPUT NAME, NOT-SO-ADDRESS-SPECIFIC
    // GOOD - decouples data and UI nicely.
    //        Their only dependency is through property name which has to be assigned as input name.
    //        We never have to change the JS code, if we only want to add another field to the form.
    // BAD - data binding function dataBind2 needs to be called each time Application.data is assigned
    //       with another address.
    // BAD - we probably shouldn't be using name attribute, as it might be used for other purposes.
    //       Typically some custom attribute is used for data bindings, like data-bind in Knockout.
    dataBind2: function() {
        document.querySelectorAll("input").forEach(function(input) {
            var property = input.getAttribute("name");
            input.value = Application.address[property] || "";
        });
        console.log("dataBind2 executed");
    },

    // Saves the changes
    save: function() {
        console.log("Address saved", Application.address);
    }

};
